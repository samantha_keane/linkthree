﻿using System.Collections;
using UnityEngine;

public class MenuScreen : UiScreen
{
    [SerializeField] GameObject LevelButtonArea;
    [SerializeField] LevelButton LevelButtonTemplate;
    [SerializeField] BoardLayoutSettings LayoutSettings;

    void Awake()
    {
        CreateModes();
    }

    void CreateModes()
    {
        for(int i = 0; i < LayoutSettings.NumLayouts; i++)
        {
            var clone = Instantiate<LevelButton>(LevelButtonTemplate, LevelButtonArea.transform);
            clone.Init(i, LayoutSettings.GetDetailsForIndex(i));
            clone.OnPlayClicked += PlayMode;
        }
    }

    void PlayMode(int layoutIndex)
    {
        StartCoroutine(OpenGameScreen(layoutIndex));
    }

    IEnumerator OpenGameScreen(int layoutIndex)
    {
        yield return ScreenManager.Instance.OpenScreen<GameScreen>();

        var gameScreen = ScreenManager.Instance.GetScreenOfType<GameScreen>();

        gameScreen.Init(layoutIndex);

        yield return ScreenManager.Instance.RemoveScreen<MenuScreen>();
    }
}
