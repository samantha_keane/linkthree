﻿using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    [SerializeField] Text ShapeTypeLabel;
    [SerializeField] Text SizeLabel;
    [SerializeField] Text MaxLinksLabel;
    [SerializeField] Text PBScoreLabel;

    public System.Action<int> OnPlayClicked;

    int LayoutIndex;

    public void Init(int layout, BoardLayoutDetails details)
    {
        LayoutIndex = layout;
        ShapeTypeLabel.text = details.Shape.ToString();
        SizeLabel.text = $"{details.Size.x}x{details.Size.y}";
        MaxLinksLabel.text = $"Max Links: {details.LinkLimit.ToString()}";
        PBScoreLabel.text = $"PB: {ScoringHelper.GetPBScoreForLayout(layout).ToString()}";
    }

    public void UiPlay()
    {
        OnPlayClicked.Invoke(LayoutIndex);
    }
}
