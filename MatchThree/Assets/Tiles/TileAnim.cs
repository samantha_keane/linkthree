﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileAnim : StateMachineBehaviour
{
    Tile RelatedTile;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        RelatedTile = animator.GetComponent<Tile>();

        var clipInfo = animator.GetCurrentAnimatorClipInfo(layerIndex);
        RelatedTile.OnAnimStarted(clipInfo[0].clip.name);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
        var clipInfo = animator.GetCurrentAnimatorClipInfo(layerIndex);     
		RelatedTile.OnAnimFinished(clipInfo[0].clip.name);
	}
}
