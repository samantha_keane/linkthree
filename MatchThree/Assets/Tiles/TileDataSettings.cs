﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum eTileType
{
    NONE = 0,
    RED = 1,
    BLUE = 2,
    GREEN = 3,
    YELLOW = 4,
    PURPLE = 5,
    WILD = 99,
}

[System.Serializable]
public struct TileData
{
    [SerializeField] public eTileType TileType;
    [SerializeField] public Sprite Visual;
}

[CreateAssetMenu(fileName = "TileDataSettings", menuName = "ScriptableObjects/TileDataSettings", order = 1)]
public class TileDataSettings : ScriptableObject
{
    public List<TileData> TileData = new List<TileData>();

    public Sprite GetVisualForType(eTileType tileType)
    {
        return TileData.Find(x=>x.TileType == tileType).Visual;
    }

    public TileData GenerateData()
    {
        int val = Random.Range(0, TileData.Count);
        return TileData[val];
    }

    public TileData GetDataByType(eTileType type)
    {
        return TileData.Find(x=>x.TileType == type);
    }
}