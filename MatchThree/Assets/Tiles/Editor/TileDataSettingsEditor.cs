﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TileDataSettings))]
public class TileDataSettingsEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var settings = target as TileDataSettings;

        var data = settings.TileData;

        if(GUILayout.Button("Reset"))
        {
            foreach(eTileType thisType in System.Enum.GetValues(typeof(eTileType)))
            {
                if (thisType != eTileType.NONE)
                {
                    data.Add(new TileData(){TileType = thisType});
                }
            }
        }

        for(int i = 0; i < data.Count; i++)
        {
            var thisData = data[i];
            EditorGUILayout.LabelField("Type: ", thisData.TileType.ToString());
            thisData.Visual = (Sprite)EditorGUILayout.ObjectField(thisData.Visual, typeof(Sprite), allowSceneObjects:false);

            data[i] = thisData;
        }

        EditorUtility.SetDirty(target);
    }
}
