﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tile : MonoBehaviour
{
    enum eStatus
    {
        NONE = 0,
        NORMAL = 1,
        DISABLED = 2,
        SELECTED = 3,
        CONSUMED = 4,
        EMPTY = 5,
        FILLING = 6,
        NO_DATA = 7,
    }

    readonly static string TRIGGER_CONSUME = "Consume";
    readonly static string TRIGGER_NORMAL = "Normal";
    readonly static string TRIGGER_FILL = "Fill";
    readonly static string TRIGGER_SELECTED = "Selected";
    readonly static string TRIGGER_VALID_SELECT = "ValidSelect";
    readonly static string TRIGGER_DISABLED = "Disabled";
    readonly static string TRIGGER_EMPTY = "Empty";

    [SerializeField] Image Visual;
    [SerializeField] Image Bg;
    [SerializeField] Animator Anim;
    [SerializeField] float BaseMovementDuration;
    [SerializeField] AnimationCurve MovementCurve;
    [SerializeField] Text Score;
    [SerializeField] RectTransform Arrow;

    public RectTransform Rect { get { return transform as RectTransform; } }

    public eTileType Type { get; private set; }
    public bool IsEmpty { get { return CurrentStatus == eStatus.EMPTY; } }
    public bool HasNoData { get { return CurrentStatus == eStatus.NO_DATA; } }
    public bool IsMoving { get; private set; }
    [HideInInspector] public List<Vector2> FillMovements = new List<Vector2>();

    eStatus CurrentStatus = eStatus.NO_DATA;
    
    public void Init(TileData data)
    {
        Type = data.TileType;
        Visual.sprite = data.Visual;
        SetStatus(eStatus.NORMAL);
    }

    public void Fill(TileData data)
    {
        Init(data);
        TriggerAnim(TRIGGER_FILL);
        SetStatus(eStatus.FILLING);
    }

    public void Selected()
    {
        SetStatus(eStatus.SELECTED);
        TriggerAnim(TRIGGER_SELECTED);
    }

    public void ValidSelected()
    {
        TriggerAnim(TRIGGER_VALID_SELECT);
    }

    public void Unselected()
    {
        SetStatus(eStatus.NORMAL);
        TriggerAnim(TRIGGER_NORMAL);
    }

    public void Consume(int score)
    {
        Score.text = score == 0 ? " " : score.ToString();

        SetStatus(eStatus.CONSUMED);
        TriggerAnim(TRIGGER_CONSUME);

        UnlinkArrow();
    }

    public void EnsureEmptyStatusSet()
    {
        if (CurrentStatus != eStatus.EMPTY)
        {
            SetStatus(eStatus.EMPTY);
        }
    }

    public void SetEmpty()
    {
        if(CurrentStatus != eStatus.EMPTY && CurrentStatus != eStatus.FILLING)
        {
            SetStatus(eStatus.EMPTY);
            TriggerAnim(TRIGGER_EMPTY);
        }
    }

    public void SetDisabled()
    {
        SetStatus(eStatus.DISABLED);
        TriggerAnim(TRIGGER_DISABLED);
    }

    public void SetNormal()
    {
        SetStatus(eStatus.NORMAL);
        TriggerAnim(TRIGGER_NORMAL);
    }

    public void SetNoData()
    {
        SetStatus(eStatus.NO_DATA);
        Type = eTileType.WILD;
    }

    public void LinkedTo(Vector2 fromTile, Vector2 toTile)
    {
        var delta = fromTile - toTile;
        var angle = Mathf.Atan2(delta.x, delta.y);

        angle *= Mathf.Rad2Deg;
        angle += 90;

        Arrow.localRotation = Quaternion.Euler(0, 0, -angle);

        var distance = Vector2.Distance(fromTile, toTile);

        Arrow.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, distance);
        Arrow.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Rect.rect.width/4);

        Arrow.localScale = Vector2.one;
    }

    public void UnlinkArrow()
    {
        Arrow.localScale = Vector2.zero;
    }

    public void OnAnimStarted(string animName)
    {
        if(CurrentStatus == eStatus.FILLING && (animName == TRIGGER_EMPTY || animName == TRIGGER_NORMAL))
        {
            SetStatus(eStatus.NORMAL);
            Anim.ResetTrigger(TRIGGER_FILL);
        }
        else if (CurrentStatus == eStatus.CONSUMED && animName == TRIGGER_EMPTY)
        {
            SetStatus(eStatus.EMPTY);
        }
    }

    public void OnAnimFinished(string animName)
    {
        if (CurrentStatus == eStatus.CONSUMED && animName == TRIGGER_EMPTY)
        {
            SetStatus(eStatus.EMPTY);
        }
        else if (CurrentStatus == eStatus.FILLING && (animName == TRIGGER_NORMAL || animName == TRIGGER_FILL))
        {
            SetStatus(eStatus.NORMAL);
            Anim.ResetTrigger(TRIGGER_FILL);
        }
    }

    public bool CanLink(eTileType selectTileType = eTileType.NONE)
    {
        var isValidStatus = CurrentStatus == eStatus.NORMAL;
        var selectedTileTypeMatches = selectTileType == Type || selectTileType == eTileType.NONE;

        var canLink = isValidStatus && selectedTileTypeMatches;

        return canLink;
    }

    public bool CanLinkByType(eTileType type, bool useWildType)
    {
        var hasTypeNone = type == eTileType.NONE;
        var isMatchingType = Type == type; 
        var isWildType = useWildType && Type == eTileType.WILD;

        return !hasTypeNone && (isMatchingType || isWildType);
    }

    public bool CanUnlink()
    {
        return CurrentStatus == eStatus.SELECTED;
    }

    public IEnumerator<YieldInstruction> ActivateFillMovement()
    {
        if(FillMovements.Count < 1)
            yield break;

        IsMoving = true;

        var MovementDuration = BaseMovementDuration * FillMovements.Count;
        var time = 0f;
        var tPerTile = 1f / FillMovements.Count;

        FillMovements.Insert(0, transform.localPosition);

        while(time < MovementDuration)
        {
            time += Time.deltaTime;
            var t = MovementCurve.Evaluate(time/MovementDuration);

            var startPosIndex = Mathf.Clamp(Mathf.FloorToInt(t/tPerTile), 0, FillMovements.Count-2);
            var targetPosIndex  = startPosIndex + 1;

            var startPosNodeProp = tPerTile * startPosIndex;
            var targetPosNodeProp = tPerTile * targetPosIndex;
            var tPerNode = Mathf.InverseLerp(startPosNodeProp, targetPosNodeProp, t);

            var startPos = FillMovements[startPosIndex];
            var targetPos = FillMovements[targetPosIndex];
            
            var previousPos = transform.localPosition;

            transform.localPosition = Vector2.Lerp(startPos, targetPos, tPerNode);

            yield return null;
        }

        FillMovements.Clear();
        IsMoving = false;

        yield return null;
    }

    void SetStatus(eStatus status)
    {
        CurrentStatus = status;
    }

    void TriggerAnim(string trigger)
    {
        ResetTriggers();
        Anim.SetTrigger(trigger);
    }

    void ResetTriggers()
    {
        Anim.ResetTrigger(TRIGGER_CONSUME);   
        Anim.ResetTrigger(TRIGGER_FILL);
    }
}
