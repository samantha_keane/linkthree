﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum eLayoutShape
{
    NONE = 0,
    GRID = 1,
    PYRAMID = 2,
}

[System.Serializable]
public class SharedBoardLayoutDetails
{
    public int BaseScore;
    public int MinLinks;
}

[System.Serializable]
public class BoardLayoutDetails
{ 
    public eLayoutShape Shape;
    public Vector2Int Size;
    public int LinkLimit;
}

[System.Serializable]
public class BoardLayout
{
    public BoardLayoutDetails Details;
    public int TallestColumn;
    public List<int> Board = new List<int>();
    public List<int> FeedingColumns = new List<int>();
}

[CreateAssetMenu(fileName = "BoardLayoutSettings", menuName = "ScriptableObjects/BoardLayoutSettings", order = 1)]
public class BoardLayoutSettings : ScriptableObject
{
    [SerializeField] SharedBoardLayoutDetails SharedDetails;
    [SerializeField] List<BoardLayoutDetails> Layouts = new List<BoardLayoutDetails>();

    BoardLayout CurrentLayout;

    public int MinLinks { get { return SharedDetails.MinLinks; } }
    public int BaseScore { get { return SharedDetails.BaseScore; } }
    public int NumLayouts { get { return Layouts.Count; } }

    public BoardLayoutDetails GetDetailsForIndex(int index)
    {
        if (index < Layouts.Count)
            return Layouts[index];
        
        return null;
    }

    public BoardLayout GetLayout(int layoutIndex)
    {
        if(layoutIndex >= Layouts.Count)
            return null;
        var layout = Layouts[layoutIndex];

        switch (layout.Shape)
        {
            case eLayoutShape.GRID:
                CurrentLayout = CreateGridBoard(layout);
                break;
            case eLayoutShape.PYRAMID:
                CurrentLayout = CreatePyramidBoard(layout);
                break;
            default:
                Debug.LogError($"The shape type has not been set for layout index {layoutIndex}.");
                CurrentLayout = null;
                break;
        }

        if(CurrentLayout != null)
        {
            CurrentLayout.TallestColumn = layout.Size.y;
            CurrentLayout.Details = layout;
        }

        return CurrentLayout;
    }

    public bool IsFeedingColumn(int columnIndex)
    {
        if(CurrentLayout == null)
            return false;

        return CurrentLayout.FeedingColumns.Contains(columnIndex);
    }

    BoardLayout CreateGridBoard(BoardLayoutDetails details)
    {
        var boardLayout = new BoardLayout();

        for(int x = 0; x < details.Size.x; x++)
        {
            boardLayout.Board.Add(details.Size.y);
            boardLayout.FeedingColumns.Add(x);
        }

        return boardLayout;
    }

    BoardLayout CreatePyramidBoard(BoardLayoutDetails details)
    {
        var boardLayout = new BoardLayout();

        var height = details.Size.y;
        var width = details.Size.x;
        var middleColumn = (width-1f)/2f;

        for (int x = 0; x < width; x++)
        {
            var distanceFromMiddleColumn = Mathf.FloorToInt(Mathf.Abs(x - middleColumn));
            var numTilesInColumn = height - distanceFromMiddleColumn;

            boardLayout.Board.Add(numTilesInColumn);

            if(numTilesInColumn == height)
            {
                boardLayout.FeedingColumns.Add(x);
            }
        }

        return boardLayout;
    }

}
