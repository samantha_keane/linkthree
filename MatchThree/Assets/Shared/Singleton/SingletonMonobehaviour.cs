﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonMonobehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
    public static T Instance
    {
        get
        {
            if (instance == null)
                instance = (T)FindObjectOfType(typeof(T));

            if (instance == null)
            {
                CreateInstance();
            }

            return instance;
        }
    }

    static void CreateInstance()
    {
        var obj = new GameObject();
        instance = obj.AddComponent<T>();
        obj.name = typeof(T).ToString() + "_Singleton";

        DontDestroyOnLoad(obj);
    }

    private static T instance;

}
