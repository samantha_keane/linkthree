﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ScoringHelper
{
    static string ScoreKey { get { return $"LayoutScore_"; } }

    public static int GetPBScoreForLayout(int layoutIndex)
    {
        var scoreKey = $"{ScoreKey}{layoutIndex}";
        var score = PlayerPrefs.GetInt(scoreKey);
        return score;
    }

    public static void SetPBScoreForLayout(int layoutIndex, int score)
    {
        var scoreKey = $"{ScoreKey}{layoutIndex}";
        PlayerPrefs.SetInt(scoreKey, score);
        PlayerPrefs.Save();
    }

    public static int GetScoreFromOrderIndex(int scoreIndex, int minLinks, int baseScore, bool separateMinLinksScore = false)
    {
        var indexDiff = scoreIndex - minLinks;

        if (indexDiff > 0)
        {
            return baseScore * indexDiff;
        }
        else
        {
            if (separateMinLinksScore)
            {
                var prop = baseScore/100;
                var minScore = 10/prop;
                var score = 0;
                for(int i = scoreIndex; i > 0; i--)
                {
                    score += minScore * i;
                }

                return score;
            }
            else if (indexDiff == 0)
            {
                return baseScore;
            }
        }

        return 0;
    }
}
