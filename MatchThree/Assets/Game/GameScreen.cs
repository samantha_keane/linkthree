﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameScreen : UiScreen
{
    [Header("Board")]
    [SerializeField] BoardLayoutSettings BoardLayoutSettings;
    [SerializeField] BoardManager Board;

    [Space]

    [Header("Details")]
    [SerializeField] Text LinkCountLabel;
    [SerializeField] Text LinkScoreLabel;
    [SerializeField] Text TotalScoreLabel;

    [Space]

    [Header("EndOfGame")]
    [SerializeField] EndOfGameView EndOfGameView;

    int LayoutIndex;
    int RemainingLinks;
    int CurrentLinkScore;
    int TotalScore;

    public void Init(int layoutIndex)
    {
        LayoutIndex = layoutIndex;

        var board = BoardLayoutSettings.GetLayout(LayoutIndex);

        Board.Init(board.Board, board.TallestColumn);

        RemainingLinks = board.Details.LinkLimit;

        Board.OnSuccessfulLink += OnSuccessfulLink;
        Board.OnTileSelected += RecalculateScore;
        Board.OnTileReleased += RecalculateScore;

        RecalculateScore(0);
        RefreshUi();
    }

    public void UiBacktoMenu()
    {
        StartCoroutine(OpenMenuScreen());
    }

    IEnumerator OpenMenuScreen()
    {
        yield return ScreenManager.Instance.OpenScreen<MenuScreen>();
        yield return ScreenManager.Instance.RemoveScreen<GameScreen>();
    }

    void OnSuccessfulLink()
    {
        RemainingLinks -= 1;

        TotalScore += CurrentLinkScore;
        CurrentLinkScore = 0;

        RecalculateScore(0);
        RefreshUi();

        if(RemainingLinks == 0)
        {
            EndGame();
        }
    }

    void RefreshUi()
    {
        LinkCountLabel.text = RemainingLinks.ToString();
        TotalScoreLabel.text = TotalScore.ToString();
    }

    void EndGame()
    {
        var previousScore = ScoringHelper.GetPBScoreForLayout(LayoutIndex);

        if(TotalScore > previousScore)
            ScoringHelper.SetPBScoreForLayout(LayoutIndex, TotalScore);

        OpenScoreView(previousScore);
    }

    void OpenScoreView(int previousScore)
    {
        EndOfGameView.gameObject.SetActive(true);
        EndOfGameView.Init(TotalScore, previousScore);
    }

    void RecalculateScore(int numSelected)
    {
        var minLinks = BoardLayoutSettings.MinLinks;
        if (numSelected < minLinks)
        {
            CurrentLinkScore = 0;
        }
        else
        {
            var baseScore = BoardLayoutSettings.BaseScore;

            var newScore = 0;
            for(int i = 0; i <= numSelected; i++)
            {
                newScore += ScoringHelper.GetScoreFromOrderIndex(i, minLinks, baseScore);
            }

            CurrentLinkScore = newScore;
        }

        LinkScoreLabel.text = CurrentLinkScore.ToString(); 
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        
        Board.OnSuccessfulLink -= OnSuccessfulLink;
        Board.OnTileSelected -= RecalculateScore;
        Board.OnTileReleased -= RecalculateScore;
    }
}
