﻿using UnityEngine;
using UnityEngine.UI;

public class EndOfGameView : MonoBehaviour
{
    [SerializeField] Text Score;
    [SerializeField] Text BestPreviousScore;
    [SerializeField] Text Message;

    public void Init(int score, int previousScore)
    {
        Score.text = score.ToString();
        BestPreviousScore.text = previousScore.ToString();

        var message = "Try Again.";
        if(score > previousScore)
        {
            message = "New Personal Best!";
        }
        Message.text = message;
    }
}
