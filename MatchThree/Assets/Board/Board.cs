﻿using System.Collections.Generic;
using UnityEngine;

public class Board
{
    public List<List<Tile>> Tiles = new List<List<Tile>>();

    public int NumColumns { get { return Tiles.Count; } }

    public int GetNumTilesInColumn(int columnIndex)
    {
        return Tiles[columnIndex].Count;
    }

    public bool AreAllTilesWild()
    {
        return Tiles.Exists(x=>x.Exists(p=>p.Type != eTileType.WILD)) == false;
    }

    public Vector2 ConvertIndexToPos(Tile tile, Vector2Int index)
    {
        var width = tile.Rect.sizeDelta.x;
        var height = tile.Rect.sizeDelta.y;

        var xPos = width * index.x;
        var yPos = height * index.y;
        return new Vector2(xPos, yPos);
    }

    public void SetTilePosition(Vector2Int index)
    {
        var tile = GetTile(index);
        tile.transform.localPosition = ConvertIndexToPos(tile, index);
    }

    public Tile GetTile(Vector2Int index)
    {
        return Tiles[index.x][index.y];
    }

    public void SetTile(Vector2Int index, Tile tile)
    {
        Tiles[index.x][index.y] = tile;
    }

    public void AddColumn(List<Tile> column)
    {
        Tiles.Add(column);
    }
}
