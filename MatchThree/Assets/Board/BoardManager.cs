﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public partial class BoardManager : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] BoardLayoutSettings BoardLayoutSettings;
    [SerializeField] TileDataSettings TileDataSettings;
    [SerializeField] Tile TileTemplate;
    [SerializeField] RectTransform BoardRect;
    [SerializeField] RectTransform BoardContainer;
    [SerializeField] BoardFillManager Filler;
    [SerializeField] float consumeNextTileDelay = 0.2f;

    public event System.Action OnSuccessfulLink;
    public event System.Action<int> OnTileSelected;
    public event System.Action<int> OnTileReleased;

    Board Board = new Board();

    List<Vector2Int> SelectedTiles = new List<Vector2Int>();
    eTileType SelectedTileType = eTileType.NONE;

    int MinLinks = 0;
    int NumTilesInHighestColumn = 0;

    bool InputBlocked;

    public void Init(List<int> columnHeights, int numTilesInHighestColumn)
    {
        InputBlocked = true;

        NumTilesInHighestColumn = numTilesInHighestColumn;
        MinLinks = BoardLayoutSettings.MinLinks;

        InitialiseBoard(columnHeights);
        Filler.GenerateData(Board);

        InputBlocked = false;
    }

    void InitialiseBoard(List<int> columnHeights)
    {
        var tileSize = CalculateTileSize(columnHeights.Count);
        TileTemplate.Rect.sizeDelta = tileSize;

        SetBoardSize(tileSize, columnHeights.Count);

        CreateTiles(columnHeights);
        PositionTiles();
    }

    Vector2 CalculateTileSize(int numColumns)
    {
        var boundsWidth = BoardContainer.rect.width;
        var boundsHeight = BoardContainer.rect.height;

        var tileWidth = boundsWidth / numColumns;
        var tileHeight = boundsHeight / NumTilesInHighestColumn;
        var tileLength = Mathf.Min(tileWidth, tileHeight);

        return new Vector2(tileLength, tileLength);
    }

    void SetBoardSize(Vector2 tileSize, int numColumns)
    {
        var boardHeight = tileSize.y * NumTilesInHighestColumn;
        var boardWidth = tileSize.x * numColumns;
        BoardRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, boardHeight);
        BoardRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, boardWidth);

        BoardRect.anchoredPosition = new Vector2(-(boardWidth / 2), -(boardHeight / 2));
    }

    void CreateTiles(List<int> columnHeights)
    {
        for(int i = 0; i < columnHeights.Count; i++)
        {
            var numRows = columnHeights[i];
            var column = new List<Tile>(numRows);

            for(int j = 0; j < numRows; j++)
            {
                var clone = Instantiate(TileTemplate, transform);
                column.Add(clone);
            }

            Board.AddColumn(column);
        }
    }

    void PositionTiles()
    {
        for(int i = 0; i < Board.NumColumns; i++)
        {
            var numRows = Board.GetNumTilesInColumn(i);
            for(int j = 0; j < numRows; j++)
            {
                Board.SetTilePosition(new Vector2Int(i, j));
            }
        }
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        if (InputBlocked)
            return;

        var tileIndex = GetBoardIndexFromScreenPos(eventData.position);
        var tile = Board.GetTile(tileIndex);

        if (tile.CanLink(SelectedTileType))
        {
            InputBlocked = true;

            SelectedTileType = tile.Type;
            SelectTile(tile, tileIndex);

            foreach(var thisColumn in Board.Tiles)
            {
                foreach(var thisTile in thisColumn)
                {
                    if(thisTile.Type != SelectedTileType)
                    {
                        thisTile.SetDisabled();
                    }
                }
            }
        }
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        if (!InputBlocked && SelectedTiles.Count <= 0)
            return;

        var tileIndex = GetBoardIndexFromScreenPos(eventData.position);

        if(tileIndex.x < 0 || tileIndex.x >= Board.NumColumns)
            return;
        if(tileIndex.y < 0 || tileIndex.y >= Board.GetNumTilesInColumn(tileIndex.x))
            return;

        var isAdjacent = false;

        if (SelectedTiles.Count > 0)
        {
            var currentTile = SelectedTiles[SelectedTiles.Count-1];
            var diff = Vector2.Distance(tileIndex, currentTile);

            isAdjacent = diff >= 1 && diff < 1.5f;
        }

        var tile = Board.GetTile(tileIndex);

        if (isAdjacent && tile.CanLink(SelectedTileType))
        {
            SelectTile(tile, tileIndex);
        }
        else if(ShouldUnselectLastTile(tileIndex, isAdjacent))
        {
            var recentLink = SelectedTiles[SelectedTiles.Count-1];
            var recentTile = Board.GetTile(recentLink);
            UnselectTile(recentTile, recentLink);
            tile.UnlinkArrow();
        }
    }

    bool ShouldUnselectLastTile(Vector2 currentTileIndex, bool isAdjacent)
    {      
        if (SelectedTiles.Count < 2)
            return false;

        if (isAdjacent)
        {
            var previousLink = SelectedTiles[SelectedTiles.Count-2];
            var isOnPreviousLink = currentTileIndex == previousLink;

            var recentLink = SelectedTiles[SelectedTiles.Count-1];
            var tile = Board.GetTile(recentLink);
            var canUnlink = tile.CanUnlink();

            return isOnPreviousLink && canUnlink;
        }

        return false;
    }

    void SelectTile(Tile tile, Vector2Int tileIndex)
    {
        SelectedTiles.Add(tileIndex);
        tile.Selected();

        var numSelectedTiles = SelectedTiles.Count;
        OnTileSelected.Invoke(numSelectedTiles);

        if(numSelectedTiles > MinLinks)
        {
            tile.ValidSelected();
        }
        else if (numSelectedTiles == MinLinks)
        {
            foreach(var thisSelectedTile in SelectedTiles)
            {
                var thisTile = Board.GetTile(thisSelectedTile);
                thisTile.ValidSelected();        
            }
        }

        if (numSelectedTiles > 1)
        {
            var previousTileIndex = SelectedTiles[SelectedTiles.Count-2];
            var previousTile = Board.GetTile(previousTileIndex);
            previousTile.LinkedTo(previousTile.transform.localPosition, tile.transform.localPosition);
        }
    }

    void UnselectTile(Tile tile, Vector2Int tileIndex)
    {
        SelectedTiles.Remove(tileIndex);
        tile.Unselected();

        var numSelectedTiles = SelectedTiles.Count;
        OnTileReleased.Invoke(numSelectedTiles);

        if (numSelectedTiles == MinLinks-1)
        {
            foreach(var thisSelectedTile in SelectedTiles)
            {
                var thisTile = Board.GetTile(thisSelectedTile);
                thisTile.Selected(); 
            } 
        }
    }

    IEnumerator ConsumeTilesCo()
    {
        var consumedTiles = new List<Vector2Int>(SelectedTiles);
        SelectedTiles.Clear();
        for(int i = 0; i < consumedTiles.Count; i++)
        {
            yield return StartCoroutine(ConsumeTile(consumedTiles[i], i, i == consumedTiles.Count-1));
        }

        SelectedTileType = eTileType.NONE;

        yield return StartCoroutine(Filler.Refill(Board, NumTilesInHighestColumn));

        yield return new WaitForEndOfFrame();

        OnSuccessfulLink.Invoke();

        InputBlocked = false;
    }

    IEnumerator ConsumeTile(Vector2Int tileIndex, int selectedIndex, bool isLastTile)
    {
        var score = ScoringHelper.GetScoreFromOrderIndex(selectedIndex+1, BoardLayoutSettings.MinLinks, BoardLayoutSettings.BaseScore, separateMinLinksScore:true);

        var tile = Board.GetTile(tileIndex);
        tile.Consume(score);

        if(isLastTile)
        {
            while (!tile.IsEmpty)
                yield return null;
        }
        else
            yield return new WaitForSeconds(consumeNextTileDelay);

        tile.EnsureEmptyStatusSet();
    }

    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        if (SelectedTiles.Count >= MinLinks)
        {
            StartCoroutine(ConsumeTilesCo());
        }
        else if (SelectedTiles.Count > 0)
        {
            foreach(var thisColumn in Board.Tiles)
            {
                foreach(var thisTile in thisColumn)
                {
                    thisTile.SetNormal();
                    thisTile.UnlinkArrow();
                }
            }
            SelectedTiles.Clear();
            SelectedTileType = eTileType.NONE;
            InputBlocked = false;
        }

    }

    Vector2Int GetBoardIndexFromScreenPos(Vector2 position)
    {
        Vector2 localPos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(BoardRect, position, null, out localPos);

        var index = localPos / TileTemplate.Rect.sizeDelta;
        int xIndex = Mathf.CeilToInt(index.x) - 1;
        int yIndex = Mathf.CeilToInt(index.y) - 1;

        return new Vector2Int(xIndex, yIndex);
    }
}
