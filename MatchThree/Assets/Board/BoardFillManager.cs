﻿using System.Collections;
using UnityEngine;

public class BoardFillManager : MonoBehaviour
{
    [SerializeField] BoardLayoutSettings BoardLayoutSettings;
    [SerializeField] TileDataSettings TileDataSettings;

    public IEnumerator Refill(Board board, int tallestColumn)
    {
        var numSpawnedTiles = new int[board.NumColumns];
        var dirty = false;
        do
        {
            dirty = false;
            for(int y = 0; y < tallestColumn; y++)
            {
                for(int x = 0; x < board.NumColumns; x++)
                {
                    if(y >= board.GetNumTilesInColumn(x))
                        continue;
                    
                    var tileIndex = new Vector2Int(x, y);
                    var tile = board.GetTile(tileIndex);
                    if(tile.IsEmpty)
                    {
                        if (TrySwapWithTilesAbove(board, tileIndex))
                        {
                            dirty = true;
                            continue;
                        }
                        
                        if (TrySpawnTile(board, tileIndex, numSpawnedTiles[tileIndex.x]))
                        {
                            dirty = true;
                            numSpawnedTiles[x]++;
                            continue;
                        }
                    }
                    else
                    {
                        tile.SetNormal();
                    }
                }
            }
        }
        while(dirty);

        GenerateData(board);

        yield return MoveTiles(board);
    }

    bool TrySwapWithTilesAbove(Board board, Vector2Int tileIndex)
    {
        var rowAboveIndex = tileIndex.y + 1;

        var tileAboveIndex = new Vector2Int(tileIndex.x, rowAboveIndex);
        if(CanUseTile(board, tileAboveIndex, isDiagonal: false))
        {
            Swap(board, tileIndex, tileAboveIndex);
            return true;
        }
        
        var tileDiagonallyLeftIndex = new Vector2Int(tileIndex.x - 1, rowAboveIndex);
        if(CanUseTile(board, tileDiagonallyLeftIndex, isDiagonal: true))
        {
            Swap(board, tileIndex, tileDiagonallyLeftIndex);
            return true;
        }
        
        var tileDiagonallyRightIndex = new Vector2Int(tileIndex.x + 1, rowAboveIndex);
        if(CanUseTile(board, tileDiagonallyRightIndex, isDiagonal: true))
        {
            Swap(board, tileIndex, tileDiagonallyRightIndex);
            return true;
        }

        return false;
    }

    bool TrySpawnTile(Board board, Vector2Int tileIndex, int numSpawnedTiles)
    {
        var rowAboveIndex = tileIndex.y + 1;

        if(BoardLayoutSettings.IsFeedingColumn(tileIndex.x) && rowAboveIndex >= board.GetNumTilesInColumn(tileIndex.x))
        {
            Spawn(board, tileIndex, numSpawnedTiles);
            return true;
        }

        return false;
    }

    public void GenerateData(Board board)
    {
        var boardAlreadyHasLink = CheckBoardForValidLinks(board);
        
        var allTilesAreWild = board.AreAllTilesWild();

        foreach(var column in board.Tiles)
        {
            foreach(var tile in column)
            {
                if(allTilesAreWild)
                {
                    allTilesAreWild = false;
                    var data = TileDataSettings.GenerateData();
                    tile.Fill(data);
                }

                if (tile.HasNoData || tile.Type == eTileType.WILD)
                {
                    if (boardAlreadyHasLink)
                    {
                        var data = TileDataSettings.GenerateData();
                        tile.Fill(data);
                    }
                    else
                    {
                        bool createdLink = TryToCreateValidLink(board);
                        if(createdLink)
                            boardAlreadyHasLink = true;
                    }
                }
            }
        }
    }

    IEnumerator MoveTiles(Board board)
    {
        foreach(var thisColumn in board.Tiles)
        {
            foreach(var thisTile in thisColumn)
            {
                StartCoroutine(thisTile.ActivateFillMovement());
            }
        }

        var isMoving = true;
        while (isMoving)
        {
            isMoving = false;
            foreach(var thisColumn in board.Tiles)
            {
                foreach(var thisTile in thisColumn)
                {
                    if (thisTile.IsMoving)
                        isMoving = true;
                }
            }
                yield return null;
        }
    }

    void Swap(Board board, Vector2Int currentTileIndex, Vector2Int nextTileIndex)
    {
        var currentTile = board.GetTile(currentTileIndex);
        var currentPos = board.ConvertIndexToPos(currentTile, currentTileIndex);

        var nextTile = board.GetTile(nextTileIndex);
        var nextPos = board.ConvertIndexToPos(nextTile, nextTileIndex);

        currentTile.FillMovements.Add(nextPos);
        nextTile.FillMovements.Add(currentPos);

        board.SetTile(currentTileIndex, nextTile);
        board.SetTile(nextTileIndex, currentTile);

        nextTile.SetNormal();
    }
    void Spawn(Board board, Vector2Int index, int numSpawnedTiles)
    {
        var tile = board.GetTile(index);

        var tileHeight = tile.Rect.sizeDelta.y;
        var tileWidth = tile.Rect.sizeDelta.x;

        var xPos = tileWidth * index.x;
        var yPos = tileHeight * index.y;

        var currentPos = new Vector3(xPos, yPos, 0);

        var columnHeight = tileHeight * board.GetNumTilesInColumn(index.x);;
        var spawnedOffset = tileHeight * numSpawnedTiles;
        var newYPos = columnHeight + spawnedOffset;
        var oldPos = currentPos;
        oldPos.y = newYPos;
            
        tile.transform.localPosition = oldPos;
        
        tile.FillMovements.Clear();

        var nextPos = oldPos;
        for(int i = 0; i <= numSpawnedTiles; i++)
        {
            nextPos.y -= tileHeight;
            tile.FillMovements.Add(nextPos);
        }

        tile.SetNoData();
    }

    bool CanUseTile(Board board, Vector2Int index, bool isDiagonal)
    {
        var tileExists = index.x < board.NumColumns && index.x >= 0 && index.y < board.GetNumTilesInColumn(index.x);
        if(tileExists == false)
            return false;

        var tile = board.GetTile(index);
        var isEmpty = tile.IsEmpty;
        if(isEmpty)
            return false;
        
        var belowIndex = index;
        belowIndex.y = index.y - 1;
        if(belowIndex.y < 0)
            return false;

        // if diagonal is being checked then we don't want to use the tile if there's a free one below.
        var tileBelow = board.GetTile(belowIndex);
        if(tileBelow.IsEmpty)
            return isDiagonal ? false : true;

        return true;
    }

    bool CheckBoardForValidLinks(Board board)
    {
        for(int x = 0; x < board.NumColumns; x++)
        {
            for(int y = 0; y < board.GetNumTilesInColumn(x); y++)
            {
                var tileIndex = new Vector2Int(x, y);
                var tile = board.GetTile(tileIndex);
                var tileType = tile.Type;
                if (tileType != eTileType.WILD)
                {
                    if (CheckAdjacentTilesForLinks(board, tileIndex, tileType))
                        return true;
                }
            }
        }
        return false;
    }

    bool CheckAdjacentTilesForLinks(Board board, Vector2Int testedTileIndex, eTileType tileType, bool useWildType = false)
    {
        var bottomLeft = testedTileIndex - Vector2Int.one;
        bottomLeft.x = Mathf.Max(bottomLeft.x, 0);
        bottomLeft.y = Mathf.Max(bottomLeft.y, 0);
        var topRight = testedTileIndex + Vector2Int.one;
        topRight.x = Mathf.Min(topRight.x, board.NumColumns-1);

        var numMatchingTiles = 0;
        for(var x = bottomLeft.x; x <= topRight.x; x++)
        {
            for(var y = bottomLeft.y; y <= topRight.y; y++)
            {
                var yIsOnBoard = y < board.GetNumTilesInColumn(x);
                if(yIsOnBoard)
                {
                    var tileIndex = new Vector2Int(x, y);
                    var tile = board.GetTile(tileIndex);
                    if(tile.Type == eTileType.NONE)
                        tile.SetNoData();

                    if(tile.CanLinkByType(tileType, useWildType))
                        numMatchingTiles++;            
                }
            }         
        }
        
        var minLinks = BoardLayoutSettings.MinLinks;
        return numMatchingTiles >= minLinks;
    }

    bool TryToCreateValidLink(Board board)
    {
        for(int x = 0; x < board.NumColumns; x++)
        {
            for(int y = 0; y < board.GetNumTilesInColumn(x); y++)
            {
                var tileIndex = new Vector2Int(x, y);
                var tile = board.GetTile(tileIndex);

                if(tile.Type == eTileType.NONE)
                {
                    var data = TileDataSettings.GenerateData();
                    tile.Fill(data);
                }
                
                if (tile.Type != eTileType.WILD)
                {
                    if (CheckAdjacentTilesForLinks(board, tileIndex, tile.Type, useWildType:true))
                    {
                        var data = TileDataSettings.GetDataByType(tile.Type);
                        FillAdjacentWildTiles(board, data, tileIndex);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    void FillAdjacentWildTiles(Board board, TileData data, Vector2Int testedTileIndex)
    {
        var bottomLeft = testedTileIndex - Vector2Int.one;
        bottomLeft.x = Mathf.Max(bottomLeft.x, 0);
        bottomLeft.y = Mathf.Max(bottomLeft.y, 0);
        var topRight = testedTileIndex + Vector2Int.one;
        topRight.x = Mathf.Min(topRight.x, board.NumColumns-1);

        var numMatchingTiles = 0;
        for(var x = bottomLeft.x; x <= topRight.x; x++)
        {
            for(var y = bottomLeft.y; y <= topRight.y; y++)
            {
                var yIsOnBoard = y < board.GetNumTilesInColumn(x);
                if(yIsOnBoard)
                {
                    var tileIndex = new Vector2Int(x, y);
                    var tile = board.GetTile(tileIndex);
                    if(tile.Type == eTileType.NONE)
                        tile.SetNoData();

                    if(tile.Type == eTileType.WILD)
                    {
                        tile.Fill(data);
                        numMatchingTiles++;
                    }
                    else if(tile.CanLinkByType(data.TileType, useWildType:true))
                    {
                        numMatchingTiles++;
                    } 
                }

                var minLinks = BoardLayoutSettings.MinLinks;
                if(numMatchingTiles >= minLinks)
                    return;
            }
        }
    }
}
