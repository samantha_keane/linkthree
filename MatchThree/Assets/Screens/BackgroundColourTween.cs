﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundColourTween : MonoBehaviour
{
    [SerializeField] Color From;
    [SerializeField] Color To;

    IEnumerator Start()
    {
        var image = GetComponent<Image>();

        for (var t = 0f; t < 1f; t += Time.deltaTime)
        {
            image.color = Color.Lerp(From, To, t);
            yield return null;
        }
        image.color = To;
    }
}
