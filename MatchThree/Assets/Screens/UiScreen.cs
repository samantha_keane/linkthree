﻿using UnityEngine;

public class UiScreen : MonoBehaviour
{
    void Start()
    {
        ScreenManager.Instance.RegisterScreen(this);
    }

    protected virtual void OnDestroy()
    {
        ScreenManager.Instance.UnRegisterScreen(this);
    }
}
