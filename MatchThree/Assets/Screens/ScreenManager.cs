﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenManager : SingletonMonobehaviour<ScreenManager>
{  
    const string SCENE_PATH = "Scenes/";

    Dictionary<System.Type, UiScreen> Screens = new Dictionary<System.Type, UiScreen>();

    public T GetScreenOfType<T>() where T : UiScreen
    {
        UiScreen value;
        Screens.TryGetValue(typeof(T), out value);
        return value as T;
    }

    public Coroutine OpenScreen<T>() where T : UiScreen
    {
        return StartCoroutine(OpenScreen_Internal<T>());
    }

    public Coroutine RemoveScreen<T>() where T : UiScreen
    {
        return StartCoroutine(RemoveScreen_Internal<T>());
    }

    public void RegisterScreen(UiScreen screen)
    {
        Screens.Add(screen.GetType(), screen);
    }

    public void UnRegisterScreen(UiScreen screen)
    {
        Screens.Remove(screen.GetType());
    }

    IEnumerator OpenScreen_Internal<T>() where T : UiScreen
    {
        var sceneName = $"{SCENE_PATH}{typeof(T)}";
        var load = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        
        while (!load.isDone && GetScreenOfType<T>() == null)
            yield return null;
    }

    IEnumerator RemoveScreen_Internal<T>() where T : UiScreen
    {
        var sceneName = $"{SCENE_PATH}{typeof(T)}";
        var unload = SceneManager.UnloadSceneAsync(sceneName, UnloadSceneOptions.None);

        while(unload != null && !unload.isDone)
             yield return null;
    }
}
